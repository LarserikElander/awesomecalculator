﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AwesomeCalculator.Core.Tests
{
    [TestClass]
    public class AppCalculatorTest
    {
        private IAppCalculator _appCalculator;

        [TestInitialize]
        public void Initialize()
        {
            _appCalculator = new AppCalculator();
        }

        [TestMethod]
        public void ShouldCalculateCorrect()
        {
            //Arrange
            int value1 = 20;
            int value2 = 10;
            int expected = 201;

            //Test
            var actual = _appCalculator.Multiply(value1, value2);

            Console.WriteLine("Calculation done. Asserting the results...");

            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}

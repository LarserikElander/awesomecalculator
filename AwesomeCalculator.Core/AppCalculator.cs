﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwesomeCalculator.Core
{
    public class AppCalculator : IAppCalculator
    {
        public int Multiply(int value1, int value2)
        {
            checked
            {
                return value1 * value2; 
            }
        }
    }
}

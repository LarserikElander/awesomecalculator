﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwesomeCalculator.Core
{
    public interface IAppCalculator
    {
        int Multiply(int value1, int value2);
    }
}

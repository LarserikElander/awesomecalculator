﻿using AwesomeCalculator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwesomeCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            int value1 = 100;
            int value2 = 200;

            IAppCalculator calculator = new AppCalculator();

            try
            {
                Console.WriteLine(
                    calculator.Multiply(value1, value2)
                );
            }
            catch (OverflowException)
            {
                Console.WriteLine("OverflowException");
            }
            catch (Exception)
            {
                Console.WriteLine("Exception");
            }
            finally
            {
                Console.ReadLine();
            }
            calculator.Multiply(value1, value2);
        }
    }
}
